//
//  main.m
//  RandomPicker
//
//  Created by Raiden Hui on 15/3/2016.
//  Copyright © 2016 RN Group. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
